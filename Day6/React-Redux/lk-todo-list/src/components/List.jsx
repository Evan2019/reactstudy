import React, {Component} from "react";
import Item from "./Item";
import {connect} from 'react-redux';

class List extends Component{
    render(){
        const {todoList} = this.props;
        return (
            <ul className="todo-main">
                {
                    todoList.map((todo, index) => (
                        <Item
                            key={index}
                            todo={todo}
                        />
                    ))
                }
            </ul>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        todoList: state.todoList
    }
};

export default connect(mapStateToProps, null)(List);
