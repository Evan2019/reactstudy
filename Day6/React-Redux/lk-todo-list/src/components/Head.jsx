import React, {Component} from "react";
import {getAddItemAction} from "../store/actionCreators";
import {connect} from 'react-redux';

class Head extends Component{
    constructor(props) {
        super(props);
        //绑定ref
        this.myInput = React.createRef();
    }

    render(){
        return (
            <div className="todo-header">
                <input
                    ref={this.myInput}
                    type="text"
                    placeholder="请输入今天的任务清单，按回车键确认"
                    onKeyDown={(e)=>this._handleEvent(e)}
                />
            </div>
        );
    }

    _handleEvent(e){
        if(e.keyCode === 13){//判断是否是回车键
            //判断输入的内容是否为空
            console.log(this.myInput.current.value);
            if(!this.myInput.current.value){
                alert('输入的内容不能为空');
                return;
            }
            const {todoList} = this.props;
            const lastTodoId = todoList.length === 0 ? 0 : todoList[todoList.length-1].id;
            //创建todo对象
            const todo = {
                id: lastTodoId + 1,
                title: this.myInput.current.value,
                finished: false
            };
            this.props.addTodo(todo);
            //清空input内容
            this.myInput.current.value = '';
        }
    }

}

const mapStateToProps = (state) => {
    return {
        todoList: state.todoList
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        addTodo(todo){
            dispatch(getAddItemAction(todo));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Head);
