import React, {Component} from "react";
import {getRemoveFinishedItemAction, getIsCheckedAll} from "../store/actionCreators";
import {connect} from 'react-redux';

class Foot extends Component{
    render(){
        const {finishedCount, todoList, dealChecked, dealClick} = this.props;
        return (
            <div className="todo-footer">
                <label>
                    <input
                        type="checkbox"
                        checked={todoList.length > 0 && finishedCount === todoList.length}
                        onChange={()=>dealChecked(finishedCount !== todoList.length)}
                    />
                </label>
                <span><span>已完成{finishedCount}件</span> / 总计{todoList.length}件</span>
                <button onClick={()=>dealClick()} className="btn btn-warning">清除已完成任务</button>
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        todoList: state.todoList,
        finishedCount: state.finishedCount
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dealChecked(flag){
            dispatch(getIsCheckedAll(flag));
        },
        dealClick(){
            dispatch(getRemoveFinishedItemAction());
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Foot);
