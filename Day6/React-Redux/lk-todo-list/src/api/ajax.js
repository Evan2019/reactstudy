import axios from 'axios';

export default function ajax(url = '', params = {}, type = 'GET'){
    let promise;
    //1.返回promise
    return new Promise((resolve, reject) => {
        //1.1 判断请求的方式
        if(type.toUpperCase() === 'GET'){
            //1.2 拼接字符串
            let paramsStr = '';
            Object.keys(params).forEach(key => {
                paramsStr += key + '=' + params[key] + '&';
            });
            //1.3 过滤
            if(paramsStr !== ''){
                paramsStr = paramsStr.substr(0, paramsStr.lastIndexOf('&'));
                //1.4 拼接完整的路径
                url += '?' + paramsStr;
            }
            //1.5 发起get请求
            promise = axios.get(url);
        }else if(type.toUpperCase() === 'POST'){
            promise = axios.post(url, params);
        }
        //1.6 返回结果
        promise.then((response)=>{
            resolve(response.data);
        }).catch(error=>{
            reject(error);
        })
    });
}
