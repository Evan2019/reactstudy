import React, {Component} from 'react';
import {version, Button} from 'antd';

import 'antd/dist/antd.css'

class AntOne extends Component{
    render(){
        return (
            <div>
                <p>Ant Design的当前版本：{version}</p>
                <Button type='danger'>百度一下</Button>
                <br/>
                <Button type='dashed'>百度一下</Button>
                <Button type='primary' icon='download' shape='circle' />
            </div>
        );
    }
}

export default AntOne;
