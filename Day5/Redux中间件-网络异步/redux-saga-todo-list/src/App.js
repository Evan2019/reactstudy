import React, {Component} from 'react';
import Head from './components/Head';
import List from './components/List';
import Foot from './components/Foot';
import store from "./store";
import {getAllItemAction} from "./store/actionCreators";


/*
基础总结：
1.声明式开发
  jQuery是命令式开发：直接操作DOM，大部分时间都是在写DOM
  React和Vue都是声明式开发：面向数据开发，相当于在盖房子时，只要把图纸画好，React就会根据图纸帮我们自动地构建大厦。节约了大量的DOM操作
2.组件化开发
  首字母大写的都是组件
3.单向数据流
  STORE ==> 组件
  好处：防止多个子组件同时修改数据
4.视图层框架
5.函数式编程
6.可以与三方框架并存
  React只负责挂载的DOM节点(id为root的div)，其它的节点可以运用其它的框架，react不会影响其使用（但要保证其它框架不会影响React的使用）
*/

/*
高阶知识点补充：
1）ReactDevelopertools
   翻墙，到谷歌应用商店安装即可
2）PropTypes和DefaultProps
   PropTypes: 设置父子组件传值的类型，用于类型的校验
   如：
   static propTypes = {
        finishedCount: PropTypes.number.isRequired,
        total: PropTypes.number.isRequired,
        delCheckedTodo: PropTypes.func.isRequired, //删除已经完成的所有任务
        dealSelectedAllTodo: PropTypes.func.isRequired, //   选中/取消所有
    };

    DefaultProps: 如果父组件没有给子组件传值，可以设置默认值
    官网：https://reactjs.org/docs/typechecking-with-proptypes.html
3）props、state和render的关系
   当组件中的state或者props发生改变时，render就会重新执行，界面就会重新被渲染。当父组件的render函数被执行时，其子组件的render函数都会被重新执行

 */

/*
Redux:
1）简介
   Redux适用于中大型项目开发进行统一的数据管理和维护
   Redux = Reducer + Flux
   Flux是React原始用于数据管理的，由于存在多个store的问题，最终升级成了Redux
2）Redux的工作流程（单向数据流）
   React Components ==> Action Creators ==> Store ==> Reducer ==> Store ==> React Components
3）使用Redux
   安装：yarn add redux
   创建store文件夹:
       index.js
       actionCreators
       actionType
       reducer


 */
class App extends Component{

    render(){
        return (
            <div className="todo-container">
                <div className="todo-wrap">
                    {/*头部*/}
                    <Head />
                    {/*列表*/}
                    <List />
                    {/*尾部*/}
                    <Foot />
                </div>
            </div>
        );
    }

    /*async _getTodoList(){
        const result = await getTodoList();
        if(result.success_code === 200){
            const action = getAllItemAction();
            store.dispatch(action);
        }
    }*/

    componentDidMount(){
        const action = getAllItemAction();
        store.dispatch(action);
    }
}

export default App;
