import React, {Component} from "react";
import Item from "./Item";
import store from "../store";

export default class List extends Component{
    constructor(props) {
        super(props);
        //获取reducer中的数据
        this.state = store.getState();

        //订阅store的改变
        this._handleStoreChange = this._handleStoreChange.bind(this);
        store.subscribe(this._handleStoreChange);
    }

    render(){
        const {todoList} = this.state;
        return (
            <ul className="todo-main">
                {
                    todoList.map((todo, index) => (
                        <Item
                            key={index}
                            todo={todo}
                        />
                    ))
                }
            </ul>
        );
    }

    //更新状态
    _handleStoreChange(){
        this.setState(store.getState());
    }
}
