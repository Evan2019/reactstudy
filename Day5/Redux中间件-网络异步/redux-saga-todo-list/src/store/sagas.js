import {put, takeEvery} from 'redux-saga/effects';
import {GET_ALL_TODO_ITEM, REQ_ALL_TODO_ITEM} from "./actionType";
import {getTodoList} from "../api";

function* getAllItem(){
    const result = yield getTodoList();
    console.log(result);
    if(result.success_code === 200){
        const todoList = result.item;
        yield put({
            type: GET_ALL_TODO_ITEM,
            todoList
        });
    }
}

function* mySaga() {
    yield takeEvery(REQ_ALL_TODO_ITEM, getAllItem);
}

export default mySaga;
