Redux-saga：
    https://github.com/redux-saga/redux-saga

概念：
    redux-saga是一个用于管理redux应用异步操作的中间件，redux-saga通过创建sagas将所有异步操作逻辑收集在一个地方集中处理
    可以用来代替redux-thunk中间件

    表现形式：
        reducer负责处理action的stage更新
        sagas负责协调那些复杂或者异步的操作

原理：
    1.sagas是通过generator函数来创建的
    2.sagas监听发起的action，然后决定基于这个action来处理
    3.在redux-saga中，所有的任务都通过用yield Effects来完成
    4.redux-saga为各项任务提供了各种Effects创建器，让我们可以用同步的方式来写异步代码
    5.名词：
      put(action):
          发起一个action到store
          创建一条Effect描述信息，指示middleware发起一个action到store
          put是异步的，不会立即发生
      takeEvery(actionTypes, func):
          监听对应type的action，一旦触发action，就会执行相应的逻辑，然后由UI组件从reducer中获取数据，并显示


运行：

使用：
