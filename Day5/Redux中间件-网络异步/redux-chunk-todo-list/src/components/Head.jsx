import React, {Component} from "react";
import store from './../store';
import {getAddItemAction} from "../store/actionCreators";

export default class Head extends Component{
    constructor(props) {
        super(props);
        //绑定ref
        this.myInput = React.createRef();
        this.state = store.getState();
        //订阅store的改变
        this._handleStoreChange = this._handleStoreChange.bind(this);
        store.subscribe(this._handleStoreChange);
    }

    render(){
        return (
            <div className="todo-header">
                <input
                    ref={this.myInput}
                    type="text"
                    placeholder="请输入今天的任务清单，按回车键确认"
                    onKeyDown={(e)=>this._handleEvent(e)}
                />
            </div>
        );
    }

    _handleEvent(e){
        if(e.keyCode === 13){//判断是否是回车键
            //判断输入的内容是否为空
            console.log(this.myInput.current.value);
            if(!this.myInput.current.value){
                alert('输入的内容不能为空');
                return;
            }
            const {todoList} = this.state;
            const lastTodoId = todoList.length === 0 ? 0 : todoList[todoList.length-1].id;
            //创建todo对象
            const todo = {
                id: lastTodoId + 1,
                title: this.myInput.current.value,
                finished: false
            };
            store.dispatch(getAddItemAction(todo));
            //清空input内容
            this.myInput.current.value = '';
        }
    }

    //更新状态
    _handleStoreChange(){
        this.setState(store.getState());
    }
}
