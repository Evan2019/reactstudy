import {DEL_TODO_ITEM, CHANGE_TODO_ITEM, ADD_TODO_ITEM, REMOVE_FINISHED_TODO_ITEM, IS_CHECKED_ALL_TODO_ITEM} from './actionType';

//默认的数据
const defaultState = {
    todoList: [
        {id: 1, title: '看一小时React的课程', finished: false},
        {id: 2, title: '跑步30分钟', finished: false},
        {id: 3, title: '打篮球2小时', finished: false},
        {id: 4, title: '散步20分钟', finished: false},
        {id: 5, title: '看书1小时', finished: false}
    ],
    finishedCount: 0
};

export default (state = defaultState, action)=>{
    console.log(state, action);
    //拷贝数据
    const newState = JSON.parse(JSON.stringify(state));
    //删除一条todo
    if(action.type === DEL_TODO_ITEM){
        let finishedCount = 0;
        let todoList = newState.todoList;
        todoList.forEach((todo, index)=>{
            if(todo.id == action.todoId){
                todoList.splice(index, 1);
            }
        });

        //处理选中的
        todoList.forEach((todo, index)=>{
            if(todo.finished){
                finishedCount += 1;
            }
        });

        newState.finishedCount = finishedCount;
        return newState;
    }
    //修改一条记录的状态
    if(action.type === CHANGE_TODO_ITEM){
        const todoList = newState.todoList;
        let finishedCount = 0;
        todoList.forEach((todo, index)=>{
            if(todo.id == action.todoId){
                todo.finished = action.isFinished;
            }
            //处理选中的
            if(todo.finished){
                finishedCount += 1;
            }
        });
        newState.finishedCount = finishedCount;
        return newState;
    }
    //添加一条记录
    if(action.type === ADD_TODO_ITEM){
        const todoList = newState.todoList;
        todoList.push(action.todo);
        return newState;
    }
    //清除所有已完成任务
    if(action.type === REMOVE_FINISHED_TODO_ITEM){
        const todoList = newState.todoList;
        let tempArr = [];
        todoList.forEach((todo, index)=>{
            if(!todo.finished){
                tempArr.push(todo);
            }
        });
        newState.todoList = tempArr;
        newState.finishedCount = 0;
        return newState;
    }
    // 选中/取消所有
    if(action.type === IS_CHECKED_ALL_TODO_ITEM){
        const todoList = newState.todoList;
        let finishedCount = action.flag ? todoList.length : 0;
        todoList.forEach((todo, index)=>{
            todo.finished = action.flag;
        });
        newState.finishedCount = finishedCount;
        return newState;
    }


    return state;
}
