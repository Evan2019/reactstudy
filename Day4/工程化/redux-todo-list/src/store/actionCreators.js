import {DEL_TODO_ITEM, CHANGE_TODO_ITEM, ADD_TODO_ITEM, REMOVE_FINISHED_TODO_ITEM, IS_CHECKED_ALL_TODO_ITEM} from './actionType';

//删除一条记录
export const getDelItemAction = (todoId) => ({
    type: DEL_TODO_ITEM,
    todoId
});

//修改一条记录的状态
export const getChangeItemFinishedAction = (todoId, isFinished) => ({
    type: CHANGE_TODO_ITEM,
    todoId,
    isFinished
});

//添加一条记录
export const getAddItemAction = (todo) => ({
    type: ADD_TODO_ITEM,
    todo
});

//清除所有已完成任务
export const getRemoveFinishedItemAction = () => ({
    type: REMOVE_FINISHED_TODO_ITEM
});

// 选中/取消所有
export const getIsCheckedAll = (flag) => ({
    type: IS_CHECKED_ALL_TODO_ITEM,
    flag
});

