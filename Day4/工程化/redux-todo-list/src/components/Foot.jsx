import React, {Component} from "react";
import store from './../store';
import {getRemoveFinishedItemAction, getIsCheckedAll} from "../store/actionCreators";

export default class Foot extends Component{
    constructor(props) {
        super(props);
        this.state = store.getState();
        //订阅store的改变
        this._handleStoreChange = this._handleStoreChange.bind(this);
        store.subscribe(this._handleStoreChange);
    }

    render(){
        const {finishedCount, todoList} = this.state;
        console.log(finishedCount, '完成数量');
        return (
            <div className="todo-footer">
                <label>
                    <input
                        type="checkbox"
                        checked={todoList.length > 0 && finishedCount === todoList.length}
                        onChange={()=>this._dealChecked(finishedCount !== todoList.length)}
                    />
                </label>
                <span><span>已完成{finishedCount}件</span> / 总计{todoList.length}件</span>
                <button onClick={()=>this._dealClick()} className="btn btn-warning">清除已完成任务</button>
            </div>
        );
    }

    _dealChecked(flag){
        store.dispatch(getIsCheckedAll(flag));
    }

    _dealClick(){
        store.dispatch(getRemoveFinishedItemAction());
    }

    //更新状态
    _handleStoreChange(){
        this.setState(store.getState());
    }
}
