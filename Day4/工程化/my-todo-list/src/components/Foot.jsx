import React, {Component} from "react";
import PropTypes from "prop-types";

export default class Foot extends Component{

    static propTypes = {
        finishedCount: PropTypes.number.isRequired,
        total: PropTypes.number.isRequired,
        delCheckedTodo: PropTypes.func.isRequired, //删除已经完成的所有任务
        dealSelectedAllTodo: PropTypes.func.isRequired, //   选中/取消所有
    };

    render(){
        const {finishedCount, total, delCheckedTodo, dealSelectedAllTodo} = this.props;
        console.log(finishedCount, '完成数量');
        return (
            <div className="todo-footer">
                <label>
                    <input
                        type="checkbox"
                        checked={finishedCount === total}
                        onChange={()=>dealSelectedAllTodo(finishedCount !== total)}
                    />
                </label>
                <span><span>已完成{finishedCount}件</span> / 总计{total}件</span>
                <button onClick={()=>delCheckedTodo()} className="btn btn-warning">清除已完成任务</button>
            </div>
        );
    }
}
