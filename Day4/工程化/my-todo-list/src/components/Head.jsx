import React, {Component} from "react";
import PropTypes from 'prop-types';

export default class Head extends Component{
    constructor(props) {
        super(props);

        //绑定ref
        this.myInput = React.createRef();
    }

    static propTypes = {
        lastTodoId: PropTypes.number.isRequired, //最后一条记录的ID
        addOneTodo: PropTypes.func.isRequired, //添加一条todo
    };

    render(){
        return (
            <div className="todo-header">
                <input
                    ref={this.myInput}
                    type="text"
                    placeholder="请输入今天的任务清单，按回车键确认"
                    onKeyDown={(e)=>this._handleEvent(e)}
                />
            </div>
        );
    }

    _handleEvent(e){
        if(e.keyCode === 13){//判断是否是回车键
            //判断输入的内容是否为空
            console.log(this.myInput.current.value);
            if(!this.myInput.current.value){
                alert('输入的内容不能为空');
                return;
            }
            const {lastTodoId, addOneTodo} = this.props;
            //创建todo对象
            const todo = {
                id: lastTodoId + 1,
                title: this.myInput.current.value,
                finished: false
            };
            addOneTodo(todo);
            //清空input内容
            this.myInput.current.value = '';
        }
    }
}
