import React, {Component} from "react";
import PropTypes from 'prop-types';
import Item from "./Item";

export default class List extends Component{

    static propTypes = {
        todoList: PropTypes.array.isRequired, //数据数组
        removeTodoWithId: PropTypes.func.isRequired, //删除一条记录
        changeTodoFinished: PropTypes.func.isRequired //修改完成状态
    };

    render(){
        const {todoList, removeTodoWithId, changeTodoFinished} = this.props;
        return (
            <ul className="todo-main">
                {
                    todoList.map((todo, index) => (
                        <Item
                            key={index}
                            todo={todo}
                            removeTodoWithId={removeTodoWithId}
                            changeTodoFinished={changeTodoFinished}
                        />
                    ))
                }
            </ul>
        );
    }
}
